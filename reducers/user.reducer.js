import {SHOW_USER} from '../constants/user.constants'
export const defaultState = {}

export const ReducerFunction = (state = defaultState, action) => {
    switch (action.type) {
        case SHOW_USER:
            let newState = {
                name: action.data.name,
                surname:action.data.surname,
                password:action.data.password,
                isSelected:action.data.isSelected,
                isEnabled:action.data.isEnabled
            }
            return newState;
        default:
            return state;
    }
};
