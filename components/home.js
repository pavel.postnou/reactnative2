import { StyleSheet, Text, View,TextInput, TouchableHighlight} from 'react-native';
import React from 'react';



export default function HomeScreen({ navigation }) {
    return (
      <View style={styles.container}>
        <Text style={{ ...styles.titleText }}>Введите логин</Text>
        <TextInput style={styles.textInput} placeholder="логин" />
        <Text style={{ ...styles.titleText }}>Введите пароль</Text>
        <TextInput style={styles.textInput} placeholder="пароль" />
        <TouchableHighlight
          style={styles.touch}
          onPress={() => navigation.navigate('Reg')}>
          <Text style={styles.ref}>Ссылка на регистрацию</Text>
        </TouchableHighlight>
      </View>
    );
  }

  const styles = StyleSheet.create({
    touch: {
      marginTop: 50
    },
    ref: {
      textDecorationLine: "underline",
      color: "blue",
    },
    titleText: {
      color: "blue",
      fontSize: 20
    },
    textInput: {
      height: 30,
      width: 150,
      borderColor: 'red',
      borderWidth: 3
    },
    container: {
      flex: 1,
      backgroundColor: '#d1d1d1',
      alignItems: "center",
      justifyContent:"center"
    },
  });