import {StyleSheet ,Text, View, Button,TouchableHighlight} from 'react-native';
import React from 'react';


export default function MainScreen({ navigation }) {
    return (
      <View style={styles.container}>
        <Button onPress={() => navigation.navigate('Reg', { info: true })} title="Посмотреть информацию" />
        <TouchableHighlight
          style={styles.touch}
          onPress={() => navigation.navigate('Home')}>
          <Text style={styles.ref}>LogOut</Text>
        </TouchableHighlight>
      </View>
    );
  }

  const styles = StyleSheet.create({

    touch: {
      marginTop: 50
    },
    ref: {
      textDecorationLine: "underline",
      color: "blue"
    },
    container: {
      flex: 1,
      backgroundColor: '#d1d1d1',
      alignItems: "center",
      justifyContent: "center"
    },
  });