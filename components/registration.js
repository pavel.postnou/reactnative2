import { Alert,StyleSheet, StatusBar, Text, View, Button, TextInput, Switch, CheckBox } from 'react-native';
import React, {useContext, useEffect, useState } from 'react';
import Context from '../utils/context';

export default function RegScreen({ navigation, route }) {

    const {info} = route.params
    const [name, setName] = useState('');
    const [surname, setSurname] = useState('');
    const [password, setPassword] = useState("");
    const [isEnabled, setIsEnabled] = useState(false);
    const toggleSwitch = () => setIsEnabled(previousState => !previousState);
    const [isSelected, setSelection] = useState(false);
    const context = useContext(Context)
    console.log(context)
    console.log(info)
    useEffect(() => {
        if(info) {
            console.log("hello"),
            setName(context.userState.name),
            setSurname(context.userState.surname),
            setPassword(context.userState.password),
            setIsEnabled(context.userState.isEnabled),
            setSelection(context.userState.isSelected)
        }
    }, [info])

      return (
        <View style={styles.container}>
          <Text style={{ ...styles.mainTitle }}>Second Native</Text>
          <StatusBar style="auto" />
          <Text style={{ ...styles.titleText }}>Введите имя</Text>
          <TextInput style={styles.textInput} onChangeText={text => setName(text)} placeholder="введите имя" value={name} />
          <Text style={{ ...styles.titleText }}>Введите фамилию</Text>
          <TextInput style={styles.textInput} placeholder="введите фамилию" onChangeText={text => setSurname(text)} value={surname} />
          <Text style={{ ...styles.titleText }}>Введите пароль</Text>
          <TextInput style={styles.textInput} placeholder="введите пароль" onChangeText={text => setPassword(text)} value={password} />
          <Text style={{ ...styles.titleText }}>Получать рассылку?</Text>
          <Switch trackColor={{ false: '#d90d0d', true: '#00059c' }} thumbColor={isEnabled ? '#d90d0d' : '#00059c'}
            onValueChange={toggleSwitch} value={isEnabled} />
          <Text style={{ ...styles.titleText }}>Довольны ли вы сервисом?</Text>
          <CheckBox value={isSelected} onValueChange={setSelection} style={styles.checkbox} />
          <Button onPress={() => (name === "" | surname === "" | password === "") ?
       Alert.alert("Не все поля заполнены") : (context.handleShowUser({ name, surname, password, isEnabled, isSelected }), navigation.navigate('Main'))} title={info ? "Главная" : "Сохранить"}/>
        </View>
      );
    }
    
    
  


  const styles = StyleSheet.create({

    touch: {
      marginTop: 50
    },
    ref: {
      textDecorationLine: "underline",
      color: "blue",
    },
    mainTitle: {
      fontSize: 50,
      fontWeight: "bold",
      color: "red"
    },
    titleText: {
      color: "blue",
      fontSize: 20
    },
    textInput: {
      height: 30,
      width: 150,
      borderColor: 'red',
      borderWidth: 3
    },
    container: {
      flex: 1,
      backgroundColor: '#d1d1d1',
      alignItems: "center",
      justifyContent: "space-evenly"
    },
    centeredView: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 22,
    },
    openButton: {
      backgroundColor: 'red',
      borderRadius: 20,
      padding: 10,
      elevation: 2,
    },
    textStyle: {
      color: 'white',
      fontWeight: 'bold',
      textAlign: 'center',
    },
    modalText: {
      marginBottom: 15,
      color: "blue",
      textAlign: 'center',
    },
  });