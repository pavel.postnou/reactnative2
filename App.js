import React, {useReducer} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Context from "./utils/context";
import * as ACTIONS from "./actionCreators/users.action";
import {ReducerFunction, defaultState} from "./reducers/user.reducer";
import HomeScreen from './components/home';
import MainScreen from './components/main';
import RegScreen from './components/registration';

export default function App() {
  const Stack = createNativeStackNavigator();
  const [stateUser, dispatchUserReducer] = useReducer(ReducerFunction, defaultState);

    const handleShowUser = (data) => {
        dispatchUserReducer(ACTIONS.showUser(data));
    };

  return (
    <Context.Provider value={{
      userState: stateUser,
      handleShowUser: (data) => handleShowUser(data)
  }} >
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home">
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen name="Reg" component={RegScreen} initialParams={{info:false}} />
          <Stack.Screen name="Main" component={MainScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </Context.Provider>
  );
}